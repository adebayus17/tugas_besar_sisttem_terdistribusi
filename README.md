# Tugas Besar Sistem Terdistribusi

Pengklasifikasian jenis bunga dengan RPC

# Fungsionalitas : 
    1. Server   : 
    
            -Tempat Procedure
    2. Client   : 
    
            - Untuk memanggil procedure di server
                          
# Pembagian Tugas :
    1. Ade      :  1. bikin repository gitlab 
                   2. membuat server,client dan sebagian rumus
                   
    2. Jokie    :  1. menambahkan rumus dan fungsi 
    
    3. Deri     :  1. menambah coding main() yang nantinya akan di panggil di 
                      client     

# Alur Program 
    1. Admin mengaktifkan server 
    2. client memasukan pranjang dan lebar 
    3. Client request procedure main()
    4. Server mengirimkan hasil dari procedure main()
    5. Client menerima hasil dari procedure main()
    

