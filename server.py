from xmlrpc.server import SimpleXMLRPCServer

from xmlrpc.server import SimpleXMLRPCRequestHandler
import xmlrpc.client;
import threading

import csv
import statistics
import math


class RequestHandler(SimpleXMLRPCRequestHandler):
	rpc_paths = ('/RPC2',)

def file_download(temp):
    # buka file bernama "file_didownload.txt"
    with open(temp + ".png", 'rb') as file:
        # kirimkan file tersebut dalam bentuk xml dengan cara memanggil xmlrpc.client.Binary()
        return xmlrpc.client.Binary(file.read())

with SimpleXMLRPCServer(('192.168.50.9', 4444), requestHandler=RequestHandler,allow_none=True) as s :
    s.register_introspection_functions()

    # ====================================Sentosa=================================================
    # -----Note-----
    # C1 = Sentosa | C2 = VersiColor 
    # n1 = jumlah data sentosa 

    sSentosa = open('sSentosa.csv').read()
    C1 = [row.split(',') for row in sSentosa.split('\n')]
    # float(logS)
    # print(logS[49][1])

    numrowsS = len(C1)
    numcolsS = len(C1[0])
    
    n1 = numrowsS - 1
    
    # print(n1)

    array_sepal_length = []
    array_sepal_width = []

    array_length_sentosa= []
    for x in range(0,n1):
    	nilai = C1[x][0]
    	nilai_barisC1 = float(nilai)
    	array_sepal_length.append(nilai_barisC1)
    	array_length_sentosa.append(nilai_barisC1)
    	# total_barisC1 = total_barisC1 + nilai_barisC1
    # print(total_barisC1)
    # print((statistics.stdev(array_sepal_length)))


    array_width_sentosa=[]
    for x in range(0,n1):
    	nilai = C1[x][1]
    	anilai = float(nilai)
    	array_sepal_width.append(anilai)
    	array_width_sentosa.append(anilai)
    	# total_kolomC1 = total_kolomC1 + anilai
    # print(total_kolomC1)

   

    # ====================================VersiColor=================================================

    sVersicolor = open('sVersicolor.csv').read()
    C2 = [row.split(',') for row in sVersicolor.split('\n')]
    # print(logV[0][0])

    numrowV = len(C2)
    numcolsV = len(C2[0])

    n2 = numrowV - 1

    array_length_versicolor = []
    for x in range(0,n2):
    	nilai = C2[x][0]
    	nilai_barisC2 = float(nilai)
    	array_sepal_length.append(nilai_barisC2)
    	array_length_versicolor.append(nilai_barisC2)
    	# total_barisC2 = total_barisC2 + nilai_barisC2
    # print(nilai)
    # print((statistics.stdev(array_sepal_length)))


    array_width_versicolor=[]
    for x in range(0,n1):
    	nilai = C2[x][1]
    	anilai = float(nilai)
    	array_sepal_width.append(anilai)
    	array_width_versicolor.append(anilai)
    	# total_kolomC2 = total_kolomC2 + anilai
    # print(total_kolomC1)
    # print((statistics.stdev(array_sepal_width)))

    total_data = n1 + n2

    # print(total_data)

    # ===================================Batas===================================================
    # | Peluang C1/sentosa dan Peluang C2/VersiColor|

    PC1 = n1 / total_data
    PC2 = n2 /total_data

    # print(PC1) 

    # ===================================Batas===================================================
    # | mu1 = rata rata sepal length , sigma1 = standardeviasi sepal length |
    mu1 = (sum(array_sepal_length))/total_data
    sigma1 = (statistics.stdev(array_sepal_length))
    mu2 = (sum(array_sepal_width))/total_data
    sigma2 = (statistics.stdev(array_sepal_width))

    # print(mu1,mu2)

    # =============================batas============================
    # | mu11 = rata rata sepal length di setosa , sigma11 = standardeviasi sepal length |
   
    mu11 = (sum(array_length_sentosa))/n1
    sigma11 = (statistics.stdev(array_length_sentosa))
    mu12 = (sum(array_length_versicolor))/n2
    sigma12 = (statistics.stdev(array_length_versicolor))

    # =============================batas============================
    # | mu11 = rata rata sepal widt di setosa , sigma11 = standardeviasi sepal widt |

    mu21 = (sum(array_width_sentosa))/n1
    sigma21 = (statistics.stdev(array_width_sentosa))
    mu22 = (sum(array_width_versicolor))/n2
    sigma22 = (statistics.stdev(array_width_versicolor))
    # =============================batas============================

    # lock = threading.Lock()

    # =============================batas============================

    def peluangX1(x1):
    	sqrt = 1/math.sqrt(2*3.14*sigma1)
    	exp = math.exp(-0.5*(x1 - mu1)**2/sigma1**2)
    	PX1 = sqrt*exp
    	# print(PX1)
    	return PX1


    def peluangX2(x2):
    	sqrt = 1/math.sqrt(2*3.14*sigma2)
    	exp = math.exp(-0.5*(x2 - mu2)**2/sigma2**2)
    	PX2 = sqrt*exp
    	# print(PX2)
    	return PX2

    # =============================batas============================


    def peluangX1C1(x1):
    	sqrt = 1/math.sqrt(2*3.14*sigma11)
    	exp = math.exp(-0.5*(x1 - mu11)**2/sigma11**2)
    	PX1C1 = sqrt*exp
    	# print(PX1C1)
    	return PX1C1

    def peluangX1C2(x1):
    	sqrt = 1/math.sqrt(2*3.14*sigma12)
    	exp = math.exp(-0.5*(x1 - mu12)**2/sigma12**2)
    	PX1C2 = sqrt*exp
    	# print(PX1C2)
    	return PX1C2

    # =============================batas============================


    def peluangX2C1(x2):
    	sqrt = 1/math.sqrt(2*3.14*sigma21)
    	exp = math.exp(-0.5*(x2 - mu21)**2/sigma21**2)
    	PX2C1 = sqrt*exp
    	# print(PX2C1)
    	return PX2C1

    def peluangX2C2(x2):
    	sqrt = 1/math.sqrt(2*3.14*sigma22)
    	exp = math.exp(-0.5*(x2 - mu22)**2/sigma22**2)
    	PX2C2 = sqrt*exp
    	# print(PX2C2)
    	return PX2C2

    # =============================batas============================


    def main(i1,i2):
    	
    	x1 = float(i1)
    	x2 = float(i2)
    	

    	a = peluangX1(x1)
    	b = peluangX2(x2)
    	c = peluangX1C1(x1)
    	d = peluangX1C2(x1)
    	e = peluangX2C1(x2)
    	f = peluangX2C2(x2)

    	# g = print(a,b,c,d,e,f)
    	# rataS = n1/total_data

    	peluangC1_X1X2 = c*e*PC1/a/b
    	peluangC2_X1X2 = d*f*PC2/a/b

    	# g = print (a,b,c,d,e,f)

    	if peluangC2_X1X2 > peluangC1_X1X2:
    		pesan = "VersiColor "
    	elif peluangC1_X1X2>peluangC2_X1X2:
    		pesan =  "Setosa "

    	# print("px1 ", a)
    	# print("px2 ", b)
    	# print("px1c1 ", c)
    	# print("px1c2 ", d)
    	# print("px2c1 ", e)
    	# print("px2c2 ", f)

    


    	return pesan
    	




    s.register_function(file_download, 'download')
    s.register_function(main,'main')
    s.register_multicall_functions()
    print ("Server berjalan...")
    s.serve_forever()
